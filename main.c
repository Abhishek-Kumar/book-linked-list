#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct book{
	struct book* start;
	char name[25];
	char author[25];
	double price;
	struct book* end;
};

struct book* traverse_to_start(struct book* sample){
	while(sample->start){
		sample = sample->start;
	}
	return sample;
}

struct book* traverse_to_last(struct book* sample){
	while(sample->end){
		sample = sample->end;
	}
	return sample;
}

struct book* first_to_position(struct book* sample, unsigned int position){
	sample = traverse_to_start(sample);
	int i;
	for(i = 1; i < position - 1; i++){
		if(sample->end) {
			sample = sample->end;	
		}else{
			sample = NULL;
			printf("\n Position doesn't exist !!\n");
			break;
		}
	}
	return sample;
}


struct book* delete_last(struct book* sample){
	sample = traverse_to_last(sample);
	sample = sample->start;
	free(sample->end);
	sample->end = NULL;
	return sample;
}

struct book* delete_first(struct book* sample){
	sample = traverse_to_start(sample);
	sample = sample->end;
	free(sample->start);
	sample->start = NULL;
	return sample;
}


struct book* delete_position(struct book* sample, int position){
	sample = first_to_position(sample, position);
	struct book *temp = sample->end;
	sample->end = temp->end;
	sample->end->start = sample;
	free(temp);
	return sample;
}

void print_record(struct book* sample){
	printf("book name: %s \n", sample->name);
	printf("author name: %s\n", sample->author);
	printf("price %lf\n", sample->price);
	return;
}

struct book* input_record(struct book* sample) {
	printf("Please enter the name of the book: \n");
	scanf("%s", sample->name);
	printf("please enter the author's name\n");
	scanf("%s", sample->author);
	printf("please enter the price of the book\n");
	scanf("%lf", &sample->price);
	return sample;	
}

struct book* create_node (){
	struct book *temp = (struct book*)malloc(sizeof(struct book));
	
	// say if the os doesn't gives us the space..
	if(!temp){
		printf("Error !! cannot create a new node!");
		return NULL;
	}
	temp->start = NULL;
	temp->end = NULL;
	strcpy(temp->name, "");
	strcpy(temp->author, "");
	temp->price = 0;
	return temp;
}

struct book* insert_last(struct book *sample){
	// check if the node we got is the last node or not if not go there

	sample =  traverse_to_last(sample);

	struct book * temp = create_node();
	temp->start = sample;
	sample->end = temp;

	// returns the last node
	return temp;
}

void create_record(struct book* sample){


	struct book* created_record;

	// manage first null record ..
	if(sample->start == NULL){
		created_record = sample;
	}else {
		created_record = insert_last(sample);
	}

	created_record = input_record(created_record);
	print_record(created_record);

}

void edit_record(struct book* sample){
	int position;
	printf("select a record no. to edit ..\n");
	scanf("%d", &position);
	sample = first_to_position(sample, position);
	printf("Book kept at position %d is: \n" , position);
	print_record(sample);
	printf("Please enter the records to overwrite \n");
	sample = input_record(sample);
	printf("New updated records are: \n");
	print_record(sample);

}

void display_record(struct book* sample){
	int position;
	printf("select a record no. to view: \n");
	scanf("%d", &position);
	sample = first_to_position(sample, position);
	printf("Book kept at position %d is: \n" , position);
	print_record(sample);
}

void delete_record(struct book* sample){

	int position;
	printf("enter a position to delete \n");
	scanf("%d", &position);

	sample = first_to_position(sample, position);
	
	// check if there is single element
	if(sample->end == NULL && sample->start == NULL){
		strcpy(sample->name, "");
		strcpy(sample->author, "");
		sample->price = 0;
	}else if (sample->end == NULL){
		delete_first(sample);
	}else if ( sample->start == NULL){
		delete_last(sample);
	}else {
		delete_position(sample, position);
	}

}

int main() {
	printf("Book record application\n");

	struct book* book1 = create_node();

	int command = 0;
	
	while(command != 5){

		printf("Please enter the operation to perform\n");
		printf("1. create a record\n");
		printf("2. edit a record\n");
		printf("3. delete a record\n");
		printf("4. display record \n");
		printf("5. exit\n");
		scanf("%d", &command);

		if (command == 1){
			create_record(book1);
		}else if (command == 2) {
			 edit_record(book1);
		}else if(command == 3){
			delete_record(book1);
		}else if(command == 4){
			 display_record(book1);
		}
	}

	return 0;
}
